package impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.net.URL;

public class AlertaReContacto {
	
	private String ARC_ENDPOINT = null;
	private Integer ARC_FLAG = 0;
	
	public AlertaReContacto()
	{
		WSProperties prop = new WSProperties();
		this.setEndpoint(prop.getProperty("ARC_ENDPOINT"));
		this.setFlag(Integer.parseInt(prop.getProperty("ARC_FLAG")));
	}
	
	public Boolean insertPAX(String loyaltyNumber, String connID)
	{
		if(this.getFlag() == 1)
		{
			try {
				String input = this.getInputString(connID, "FFN", loyaltyNumber);
				String response = this.insert(input);
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(response);
				JSONObject status = (JSONObject) json.get("serviceStatus");
				long statusCode = (Long) status.get("code");
				if(statusCode == 0)
				{
					return true;
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} catch (ParseException e) {
				System.out.println(e.getMessage());
			}
		}
		return false;
	}
	
	public Boolean insertANI(String ani, String connID)
	{
		if(this.getFlag() == 1)
		{
			try {
				String input = this.getInputString(connID, "ANI", ani);
				String response = this.insert(input);
				JSONParser parser = new JSONParser();
				JSONObject json = (JSONObject) parser.parse(response);
				JSONObject status = (JSONObject) json.get("serviceStatus");
				long statusCode = (Long) status.get("code");
				if(statusCode == 0)
				{
					return true;
				}
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} catch (ParseException e) {
				System.out.println(e.getMessage());
			}
		}
		return false;
	}
		
	public String insert(String input) throws IOException
	{
		URL url = new URL(this.getEndpoint());
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String authString = "srvivr" + ":" + "4E73f89PA";
        String authStringEnc = new String(Base64.encodeBase64(authString.getBytes()));
        conn.setRequestProperty("Authorization", "Basic " + authStringEnc);
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		
		OutputStream os = conn.getOutputStream();
		os.write(input.getBytes());
		os.flush();
	
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

		String output = br.readLine();
		conn.disconnect();
		
		return output;
	}

	public String getEndpoint() {
		return this.ARC_ENDPOINT;
	}

	public void setEndpoint(String endpoint) {
		this.ARC_ENDPOINT = endpoint;
	}

	public Integer getFlag() {
		return this.ARC_FLAG;
	}

	public void setFlag(Integer flag) {
		this.ARC_FLAG = flag;
	}
	
	public String getInputString(String connID, String indetifierType, String identification)
	{
		String input = "{ \"source\": \"IVR\",\"idContact\": \""+connID+"\",\"typeOfIdentifier\": \""+indetifierType+"\",\"identification\": \""+identification+"\"}";
		return input;
	}

}
