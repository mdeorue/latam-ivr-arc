package impl;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class WSProperties {

	private Properties properties = null;
	private String propFileName = System.getProperty("catalina.home")+"/lib/latam/arc_configuration.properties";
	
	public WSProperties()
	{
		if(System.getProperty("catalina.home") == null)
		{
			propFileName = new File("lib/arc_configuration.properties").getAbsolutePath();
		}
		this.properties = new Properties();
		try {
			System.out.println(this.propFileName);
			this.properties.load(new FileInputStream(this.propFileName));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public String getProperty(String prop)
	{
		String property = null;
		property = this.properties.getProperty(prop);
		return property;
	}
	
}

